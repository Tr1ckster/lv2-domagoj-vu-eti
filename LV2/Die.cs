﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Die
    {
        private int numberOfSides;
        private RandomGenerator Generator;
        //private Random randomGenerator; Zadatak 2
    
        //Zadatak 1
        //public Die(int numberOfSides)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //}

        //Zadatak 2
        //public Die(int numberOfSides, Random randomGenerator)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = randomGenerator;
        //}

        //Zadatak 3
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.Generator = RandomGenerator.GetInstance();
        }

        public int Roll()
        {
            int rolledNumber = Generator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
    }
}
